package controllers

import (
	"context"
	"log"
	"math"
	"strconv"
	"time"

	"github.com/punokawan/Simple-Web-Dompet-Kilat/database"
	"github.com/punokawan/Simple-Web-Dompet-Kilat/helpers"
	"github.com/punokawan/Simple-Web-Dompet-Kilat/models"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/asaskevich/govalidator.v9"
)

func GetAllProductiveIncomes(c *fiber.Ctx) error {
	productiveIncomeCollection := database.MI.DB.Collection("productiveIncomes")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	var ProductiveIncomes []models.ProductiveIncomes

	filter := bson.M{}
	findOptions := options.Find()

	if s := c.Query("s"); s != "" {
		filter = bson.M{
			"$or": []bson.M{
				{
					"name": bson.M{
						"$regex": primitive.Regex{
							Pattern: s,
							Options: "i",
						},
					},
				},
			},
		}
	}

	page, _ := strconv.Atoi(c.Query("page", "1"))
	limitVal, _ := strconv.Atoi(c.Query("limit", "10"))
	var limit int64 = int64(limitVal)

	total, _ := productiveIncomeCollection.CountDocuments(ctx, filter)

	findOptions.SetSkip((int64(page) - 1) * limit)
	findOptions.SetLimit(limit)

	cursor, err := productiveIncomeCollection.Find(ctx, filter, findOptions)
	defer cursor.Close(ctx)

	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"success": false,
			"message": "Catchphrases Not found",
			"error":   err,
		})
	}

	for cursor.Next(ctx) {
		var ProductiveIncome models.ProductiveIncomes
		cursor.Decode(&ProductiveIncome)
		ProductiveIncomes = append(ProductiveIncomes, ProductiveIncome)
	}

	last := math.Ceil(float64(total / limit))
	if last < 1 && total > 0 {
		last = 1
	}

	result := fiber.Map{
		"data":      ProductiveIncomes,
		"total":     total,
		"page":      page,
		"last_page": last,
		"limit":     limit,
	}

	return helpers.ResponseMsg(
		c,
		fiber.StatusOK,
		true,
		"Get Productive Incomes successfully",
		result)
}

func AddProductiveIncomes(c *fiber.Ctx) error {
	productiveIncomesCollection := database.MI.DB.Collection("productiveIncomes")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	productiveIncome := new(models.ProductiveIncomes)

	if err := c.BodyParser(&productiveIncome); err != nil {
		log.Println(err)
		return helpers.ResponseMsg(
			c,
			fiber.StatusBadRequest,
			false,
			helpers.NewJError(err).Message,
			nil)
	}

	if govalidator.IsNull(productiveIncome.Name) {
		return helpers.ResponseMsg(
			c,
			fiber.StatusBadRequest,
			false,
			helpers.NewJError(helpers.ErrEmptyName).Message,
			nil)
	}

	result, err := productiveIncomesCollection.InsertOne(ctx, productiveIncome)
	if err != nil {
		return helpers.ResponseMsg(
			c,
			fiber.StatusInternalServerError,
			false,
			"Productive Income failed to insert",
			nil)
	}
	return helpers.ResponseMsg(
		c,
		fiber.StatusCreated,
		true,
		"Productive Income inserted successfully",
		result)
}

func GetProductiveIncome(c *fiber.Ctx) error {
	log.Printf("%s\n", c.Params("id"))
	productiveIncomesCollection := database.MI.DB.Collection("productiveIncomes")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	var productiveIncome models.ProductiveIncomes

	objId, err := primitive.ObjectIDFromHex(c.Params("id"))
	findResult := productiveIncomesCollection.FindOne(ctx, bson.M{"_id": objId})
	if err := findResult.Err(); err != nil {
		return helpers.ResponseMsg(
			c,
			fiber.StatusNotFound,
			false,
			"Productive Income Not found",
			nil)
	}

	err = findResult.Decode(&productiveIncome)
	if err != nil {
		return helpers.ResponseMsg(
			c,
			fiber.StatusNotFound,
			false,
			"Productive Income Not found",
			nil)
	}

	return helpers.ResponseMsg(
		c,
		fiber.StatusOK,
		true,
		"Productive Income Not found",
		productiveIncome)
}
