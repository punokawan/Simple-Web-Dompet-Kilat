package controllers

import (
	"context"
	"log"
	"strings"
	"time"

	"github.com/punokawan/Simple-Web-Dompet-Kilat/database"
	"github.com/punokawan/Simple-Web-Dompet-Kilat/helpers"
	"github.com/punokawan/Simple-Web-Dompet-Kilat/models"
	"github.com/punokawan/Simple-Web-Dompet-Kilat/security"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"gopkg.in/asaskevich/govalidator.v9"
)

var ctx = context.Background()
var user = new(models.Users)
var inputUser = new(models.Users)

func SignUp(c *fiber.Ctx) error {
	userCollection := database.MI.DB.Collection("users")
	ctx, _ := context.WithTimeout(ctx, 10*time.Second)

	if err := c.BodyParser(&inputUser); err != nil {
		log.Println(err)
		return helpers.ResponseMsg(
			c,
			fiber.StatusUnprocessableEntity,
			false,
			"Failed to parse body",
			nil)
	}

	if govalidator.IsNull(inputUser.Username) {
		return helpers.ResponseMsg(
			c,
			fiber.StatusBadRequest,
			false,
			helpers.NewJError(helpers.ErrEmptyUsername).Message,
			nil)
	}

	if govalidator.IsNull(inputUser.Password) {
		return helpers.ResponseMsg(
			c,
			fiber.StatusBadRequest,
			false,
			helpers.NewJError(helpers.ErrEmptyPassword).Message,
			nil)
	}

	inputUser.Email = helpers.NormalizeEmail(inputUser.Email)
	if !govalidator.IsEmail(inputUser.Email) {
		return helpers.ResponseMsg(
			c,
			fiber.StatusBadRequest,
			false,
			helpers.NewJError(helpers.ErrInvalidEmail).Message,
			nil)
	}

	findEmail := userCollection.FindOne(ctx, bson.M{"email": inputUser.Email})
	if err := findEmail.Err(); err != nil {
		if strings.TrimSpace(inputUser.Password) == "" {
			return c.
				Status(fiber.StatusBadRequest).
				JSON(helpers.NewJError(helpers.ErrEmptyPassword))
		}

		inputUser.Password, err = security.EncryptPassword(inputUser.Password)
		if err != nil {
			return helpers.ResponseMsg(
				c,
				fiber.StatusBadRequest,
				false,
				helpers.NewJError(err).Message,
				nil)
		}

		inputUser.CreatedAt = time.Now()
		inputUser.UpdatedAt = inputUser.CreatedAt
		inputUser.ID = primitive.NewObjectIDFromTimestamp(time.Now())
		result, err := userCollection.InsertOne(ctx, inputUser)

		if err != nil {
			return helpers.ResponseMsg(
				c,
				fiber.StatusInternalServerError,
				false,
				"user failed to register",
				nil)
		}

		return helpers.ResponseMsg(
			c,
			fiber.StatusCreated,
			true,
			"user registered successfully",
			result)
	}

	return helpers.ResponseMsg(
		c,
		fiber.StatusBadRequest,
		false,
		helpers.NewJError(helpers.ErrEmailAlreadyExists).Message,
		nil)
}

func SignIn(c *fiber.Ctx) error {
	userCollection := database.MI.DB.Collection("users")
	ctx, _ := context.WithTimeout(ctx, 10*time.Second)

	if err := c.BodyParser(&inputUser); err != nil {
		log.Println(err)
		return helpers.ResponseMsg(
			c,
			fiber.StatusUnprocessableEntity,
			false,
			"Failed to parse body",
			nil)
	}

	if govalidator.IsNull(inputUser.Password) {
		return helpers.ResponseMsg(
			c,
			fiber.StatusBadRequest,
			false,
			helpers.NewJError(helpers.ErrEmptyPassword).Message,
			nil)
	}

	inputUser.Email = helpers.NormalizeEmail(inputUser.Email)

	findUser := userCollection.FindOne(ctx, bson.M{"email": inputUser.Email})
	if err := findUser.Err(); err != nil {
		return helpers.ResponseMsg(
			c,
			fiber.StatusUnauthorized,
			false,
			helpers.NewJError(helpers.ErrInvalidCredentials).Message,
			nil)
	}

	err := findUser.Decode(&user)
	if err != nil {
		return helpers.ResponseMsg(
			c,
			fiber.StatusNotFound,
			false,
			helpers.NewJError(err).Message,
			nil)
	}

	log.Printf("user : %s, \ninput: %s", user, inputUser)
	err = security.VerifyPassword(user.Password, inputUser.Password)
	if err != nil {
		return helpers.ResponseMsg(
			c,
			fiber.StatusUnauthorized,
			false,
			helpers.NewJError(helpers.ErrInvalidCredentials).Message,
			nil)
	}
	token, err := security.NewToken(user.ID.Hex())
	if err != nil {
		return helpers.ResponseMsg(
			c,
			fiber.StatusUnauthorized,
			false,
			helpers.NewJError(err).Message,
			nil)
	}
	response := fiber.Map{
		"user":         user,
		"access_token": token,
	}
	return helpers.ResponseMsg(
		c,
		fiber.StatusOK,
		true,
		"Login scuccessful",
		response)
}
