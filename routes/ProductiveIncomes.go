package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/punokawan/Simple-Web-Dompet-Kilat/controllers"
)

func ProductiveIncomeRoute(route fiber.Router) {
	route.Get("/", controllers.GetAllProductiveIncomes)
	route.Get("/:id", controllers.GetProductiveIncome)
	route.Post("/", controllers.AddProductiveIncomes)
}
