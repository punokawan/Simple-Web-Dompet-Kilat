package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type ProductiveIncomes struct {
	ID     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name   string             `json:"name,omitempty" bson:"name,omitempty"`
	Amount int32              `json:"amount,omitempty" bson:"amount,omitempty"`
	Tenor  int                `json:"tenor,omitempty" bson:"tenor,omitempty"`
	Grade  string             `json:"grade,omitempty" bson:"grade,omitempty"`
	Rate   int                `json:"rate,omitempty" bson:"rate,omitempty"`
}
