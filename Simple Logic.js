// 1. Check the string is palindrome ornot
function isPalindrome(str){
    if(typeof str !== 'string'){
        str = (str.toString()).replaceAll(",", "");
        console.log(str)
    }
    str = str.toLowerCase();
    return str === str.split('').reverse().join('');
}

console.log(isPalindrome('malam'))
console.log(isPalindrome('pagi'))
console.log(isPalindrome('abcba'))

// 2. Find prime number by range
function findPrime(a,b){
    let arr = []
    for (i = a; i <= b; i++) {
        if (i == 1 || i == 0)
            continue;
 
        flag = 1;
 
        for (j = 2; j <= i / 2; ++j) {
            if (i % j == 0) {
                flag = 0;
                break;
            }
        }

        if (flag == 1)
            arr.push(i)
    }
    return arr;
}

console.log(findPrime(11, 40))

// 3. Grouping array group into separate sub arraygroup
const arr = ['a', 'a', 'a', 'b', 'c', 'c', 'b', 'b', 'b', 'd', 'd', 'e', 'e', 'e']
function group(x){
    let temp = x
    let result = [[x[0]]]
    temp.slice(1).forEach( (i,j) => {
        if(i == temp[j]) {
          result[result.length-1].push(i);
        } else{
          result.push([i]);
        }
      });
    return result
}

console.log(group(arr))