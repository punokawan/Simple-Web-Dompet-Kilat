# Simple Web Go with Fiber and Mongo

# HOW TO RUN THE APPLICATION:

1. copy .env.example to .env
2. run: ```go run main.go```


# HOT TO DOCKERIZE THE APPLICATION:

1. copy .env.example to .env
2. change pubished ports on docker-compose.yml
3. run: ```make docker``` or ```docker-compose up --build```